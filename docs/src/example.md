# Example Usage: Overlap Factor

As a simple example of calculating the modal overlap factor, let's create
a set of sinusoidal field modes and some bitmasks:

```@example 1d
import FDMModeAnalysis

# Generate a set of 3 sinusoidal modes as a matrix
dx = 0.05
Nmodes = 3
x = 0:dx:pi
modes = reduce(hcat, [sin.(x .* i) for i = 1:Nmodes])

# Generate a pair of bitmasks, one for the whole mode, one for only half
allactive = ones(length(x))
halfactive = x .< pi / 2
nothing # hide
```

Now we can use the `overlapfactor` function to calculate the overlap
between a mode and a bitmask:

```@example 1d
FDMModeAnalysis.overlapfactor(dx, modes[:, 1], allactive),
FDMModeAnalysis.overlapfactor(dx, modes[:, 1], halfactive)
```

As expected, all of the modal power overlaps with the first bitmask,
and about half with the second (with some error due to discrete sampling).
We can also use `overlapfactor` to calculate the modal overlap for all
of the modes simultaneously:

```@example 1d
FDMModeAnalysis.overlapfactor(dx, modes, allactive),
FDMModeAnalysis.overlapfactor(dx, modes, halfactive)
```

If the modes were on a non-uniform grid then we can repeat the calculations
by simply providing a vector with the sample spacing at each point:

```@example 1d
dxv = fill(dx, length(x))
FDMModeAnalysis.overlapfactor(dxv, modes, allactive),
FDMModeAnalysis.overlapfactor(dxv, modes, halfactive)
```

We can do the same for 2D modes:

```@example 1d
# Calculate 2D modal fields
dx = 0.05
dy = 0.04
Nmodes = 3
x = 0:dx:pi
y = 0:dy:pi
dxv = fill(dx, length(x))
dyv = fill(dy, length(y))
modes = cat([[sin(x * i) * sin(y) for x in x, y in y] for i = 1:Nmodes]..., dims = 3)

# Bitmasks
allactive = ones(size(modes[:, :, 1]))
halfactive = [x < pi / 2 for x in x, y in y]

# Calculate the overlap factor for all of the modes assuming non-uniform sampling
FDMModeAnalysis.overlapfactor(dxv, dyv, modes, allactive),
FDMModeAnalysis.overlapfactor(dxv, dyv, modes, halfactive)

```

