# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests and documentation
* Implement other useful algorithms and metrics (perhaps waveguide dispersion, group velocity/index, etc)

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to support both 1D and 2D modes
* We aim to support modes on both uniformly and non-uniformly sampled grids
* We aim to be compatible with mode solutions from the [`FDMModes`](https://gitlab.com/pawelstrzebonski/FDMModes.jl) package
* We aim to minimize the amount of code and function repetition when implementing the above features
