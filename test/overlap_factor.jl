dx = 0.05
Nmodes = 3
x = 0:dx:pi
dxv = fill(dx, length(x))
mode = sin.(x)
modes = reduce(hcat, [sin.(x .* i) for i = 1:Nmodes])
allactive = ones(length(x))
halfactive = x .< pi / 2

@test FDMModeAnalysis.overlapfactor(dx, mode, allactive) == 1
@test abs(FDMModeAnalysis.overlapfactor(dx, mode, halfactive) - 0.5) < 0.01
@test FDMModeAnalysis.overlapfactor(dx, modes, allactive) == ones(Nmodes)
@test all(abs.(FDMModeAnalysis.overlapfactor(dx, modes, halfactive) .- 0.5) .< 0.01)

@test FDMModeAnalysis.overlapfactor(dxv, mode, allactive) == 1
@test abs(FDMModeAnalysis.overlapfactor(dxv, mode, halfactive) - 0.5) < 0.01
@test FDMModeAnalysis.overlapfactor(dxv, modes, allactive) == ones(Nmodes)
@test all(abs.(FDMModeAnalysis.overlapfactor(dxv, modes, halfactive) .- 0.5) .< 0.01)

dx = 0.05
dy = 0.04
Nmodes = 3
x = 0:dx:pi
y = 0:dy:pi
dxv = fill(dx, length(x))
dyv = fill(dy, length(y))
mode = [sin(x) * sin(y) for x in x, y in y]
modes = cat([[sin(x * i) * sin(y) for x in x, y in y] for i = 1:Nmodes]..., dims = 3)
allactive = ones(size(mode))
halfactive = [x < pi / 2 for x in x, y in y]

@test FDMModeAnalysis.overlapfactor(dx, dy, mode, allactive) == 1
@test abs(FDMModeAnalysis.overlapfactor(dx, dy, mode, halfactive) - 0.5) < 0.01
@test FDMModeAnalysis.overlapfactor(dx, dy, modes, allactive) == ones(Nmodes)
@test all(abs.(FDMModeAnalysis.overlapfactor(dx, dy, modes, halfactive) .- 0.5) .< 0.01)

@test FDMModeAnalysis.overlapfactor(dxv, dyv, mode, allactive) == 1
@test abs(FDMModeAnalysis.overlapfactor(dxv, dyv, mode, halfactive) - 0.5) < 0.01
@test FDMModeAnalysis.overlapfactor(dxv, dyv, modes, allactive) == ones(Nmodes)
@test all(abs.(FDMModeAnalysis.overlapfactor(dxv, dyv, modes, halfactive) .- 0.5) .< 0.01)
