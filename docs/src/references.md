# References

The code for package was originally made to calculate the "confinement
factor" of modes (that is the overlap of waveguide modes with the
waveguide core or active region in waveguide based lasers) for the
purpose of reasoning about modal selection and discrimination, as in
["Mode Engineering via Waveguide Structuring"](https://doi.org/10.1109/islc.2018.8516196)
and my MS thesis,
["Semiconductor Laser Mode Engineering via Waveguide Index Structuring"](http://hdl.handle.net/2142/102511).
The code was co-developed with my finite difference waveguide eigenmode solver
(whose code has evolved into the
[`FDMModes`](https://gitlab.com/pawelstrzebonski/FDMModes.jl)
package).
The relevance of the modal confinement factor for laser modes (for example
for calculating the modal threshold gain) can be found in
["Diode Lasers and Photonic Integrated Circuits"](https://coldren.ece.ucsb.edu/book).

As other metrics and analyses are found and implemented this page should
be updated with appropriate references.
